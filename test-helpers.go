package main

import "time"

func waitForCondition(e func() bool) bool {
	start := time.Now().Unix()

	for time.Now().Unix()-start < 5 {
		if e() {
			return true
		} else {
			time.Sleep(50 * time.Millisecond)
		}
	}
	return false
}
