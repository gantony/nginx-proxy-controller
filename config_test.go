package main

import (
	"io/ioutil"
	"log"
	"os"
	"testing"
)

// TestConfig tests config instanciation (it's really jsut an excuse to write a test...).
func TestConfig(t *testing.T) {
	pathToConfig := "path/to/config"
	config := Config{path: pathToConfig}
	if config.path != pathToConfig {
		t.Fatalf("Config path is broken")
	}
}

// This is just a learning test...
func TestConfigIsInvalidByDefault(t *testing.T) {
	pathToConfig := "path/to/config"
	config := Config{path: pathToConfig}

	if config.isValid {
		t.Fatalf("bool initialise to true!!")
	}
}

func TestFileWatcher(t *testing.T) {
	dir, err := ioutil.TempDir("", "test-file-watcher")
	if err != nil {
		t.Fatalf("Error creating temp dir for test")
	}

	configWatcher := ConfigWatcher{}

	changed := false

	configWatcher.watch(dir, func() { changed = true })

	// Test create new file
	tmpfile, err := ioutil.TempFile(dir, "test-file")
	if err != nil {
		t.Fatalf("Error creating temp file")
	}

	if !waitForCondition(func() bool { return changed }) {
		t.Fatalf("changed should be true after creatign a file")
	}

	changed = false

	// Test edit file content
	if _, err := tmpfile.Write([]byte("This is some test content")); err != nil {
		log.Fatal(err)
	}
	if err := tmpfile.Close(); err != nil {
		log.Fatal(err)
	}

	if !waitForCondition(func() bool { return changed }) {
		t.Fatalf("changed should be true after editing a file")
	}

	changed = false

	// Test file deletion
	os.Remove(tmpfile.Name()) // clean up

	if !waitForCondition(func() bool { return changed }) {
		t.Fatalf("changed should be true after deleting a file")
	}

	defer os.RemoveAll(dir)
}
