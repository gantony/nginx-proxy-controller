package main

import (
	"fmt"
	"os/exec"
)

// Nginx is a wrapper around the nginx service
// TODO: Cross platform implementation, this is developped on Ubuntu...
type Nginx struct{}

func (nginx *Nginx) Start(config string) {
	// cmd := exec.Command("nginx", "-c", config)
	fmt.Println("sudo nginx -c " + config)
	cmd := exec.Command("/bin/sh", "-c", "sudo nginx -c "+config)
	_, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return
	}
}

func (nginx *Nginx) Stop() {
	// cmd := exec.Command("sudo", "nginx", "-s", "quit")
	cmd := exec.Command("/bin/sh", "-c", "sudo nginx -s quit")
	stdout, err := cmd.Output()
	fmt.Println(stdout)

	if err != nil {
		fmt.Println(err.Error())
		return
	}
}

func (nginx *Nginx) ValidateConfig(config string) bool {
	cmd := exec.Command("nginx", "-t", "-c", config)
	_, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return false
	}

	return true
}

func (nginx *Nginx) ReloadConfig() {
	cmd := exec.Command("nginx", "-s", "reload")
	_, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return
	}
}
