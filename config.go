package main

import (
	"fmt"
	"log"
	"time"

	"github.com/radovskyb/watcher"
)

type Config struct {
	path    string
	isValid bool
}

func (config *Config) printPath() {
	fmt.Println("NGINX config files are stored in " + config.path)
}

// Here, the plan is monitor the config folder.
// On changes, reload the config in nginx and report the status somewhere.

// This can live in a NginxConfigWatcher struct (https://spf13.com/post/is-go-object-oriented/)
// that exposes its status as a field or "method" call... We don't need to get fancy with events
// as the consumer will be a REST API and the app call poll the status...
// Just need to find out how to run the actual watcher in tha background

type ConfigWatcher struct {
}

// This kicks off the file watching process in a non-blocking way.
// Ue onChange callback to handle desired behaviour on change
func (configWatcher ConfigWatcher) watch(path string, onChange func()) {
	// This seems fairly handy to use: https://github.com/radovskyb/watcher

	w := watcher.New()

	w.SetMaxEvents(1)

	go func() {
		for {
			select {
			case event := <-w.Event:
				fmt.Println(event) // Print the event's info.
				onChange()
			case err := <-w.Error:
				log.Fatalln(err)
			case <-w.Closed:
				return
			}
		}
	}()

	if err := w.Add(path); err != nil {
		log.Fatalln(err)
	}

	// This loops forever...
	go func() {
		// Start the watching process - it'll check for changes every 100ms.
		if err := w.Start(time.Millisecond * 100); err != nil {
			log.Fatalln(err)
		}
	}()

	// TODO: I should probably clean up w at some stage, maybe from a stopWatching method
	// (Can't think of whn I'd call it so can probably live without it)
}
