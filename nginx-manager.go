package main

import (
	"fmt"
	"path/filepath"
)

type NginxManager struct {
	config        string
	nginx         Nginx
	configWatcher ConfigWatcher
}

func NewNginxManager(config string) *NginxManager {
	m := new(NginxManager)
	m.config = config
	m.nginx = Nginx{}
	m.configWatcher = ConfigWatcher{}

	return m
}

func (m *NginxManager) Start() {
	m.nginx.Stop() // Jsut is canse it's previously running
	m.nginx.Start(m.config)
	m.configWatcher.watch(filepath.Dir(m.config), func() {
		if m.nginx.ValidateConfig(m.config) {
			m.nginx.ReloadConfig()
			// TODO: Sort out logging, here and in the rest of the app
			fmt.Println("NGINX config reloaded :)")
		} else {
			fmt.Println("Not reloading the NGINX config as the config is invalid!!!")
		}
	})
}
