# NGINX proxy controller

I'm impressed with NGINX :) It will be suitable for our project. Basically, if we update the configuration (even repace a port, like 9090 to 9092) and reload the config, the old "session" is left active (WebSocket connectoin on port 9090) and the new port 9092 becomes available as expected. Obviously, new connection on port 9090 no longer works.

## Running nginx docker container

```
docker run --name mynginx --mount type=bind,source=/home/antony/nginx-proxy-controller/nginx/www,target=/usr/share/nginx/html,readonly --mount type=bind,source=/home/antony/nginx-proxy-controller/nginx/conf,target=/etc/nginx/conf,readonly --add-host=host.docker.internal:host-gateway -p 80:80 -d nginx
```

## Running system nginx with a custom config
(Don't want to waste time with docker networking initially before being sure everything else is working...)
```
nginx  -c /home/antony/nginx-proxy-controller/nginx/conf/nginx.conf

# Reload config
nginx -s reload

# Test config (don't run nginx)
nginx -t -c /home/antony/nginx-proxy-controller/nginx/conf/nginx.conf
```

## Interestign links

- https://www.nginx.com/blog/deploying-nginx-nginx-plus-docker/
- https://echo.labstack.com/cookbook/websocket/
